<?php

set_error_handler(function ($errno, $errstr, $errfile, $errline) {
    throw new \Raise\Common\Exceptions\UserNoticeException($errstr);
}, E_USER_NOTICE);

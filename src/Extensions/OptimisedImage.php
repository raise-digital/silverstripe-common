<?php

namespace Raise\Extensions;

use SilverStripe\Assets\Image_Backend;
use SilverStripe\Assets\Storage\AssetStore;
use SilverStripe\Assets\Storage\DBFile;
use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Extension;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\ORM\FieldType\DBField;

class OptimisedImage extends Extension {

    public function Compressed() {
        if (in_array($this->owner->getMimeType(), ['image/jpg', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png'])) {
            $key = Config::inst()->get(\SilverStripe\Assets\Image::class, 'tinify_key');
            if (!empty($key)) {
                \Tinify\setKey($key);
                $new_variant = $this->owner->variantName(__FUNCTION__);
                $result = $this->owner->manipulateImage($new_variant, function(Image_Backend $backend) use ($new_variant) {
                    $container = $backend->getAssetContainer();
                    $source = \Tinify\fromBuffer($container->getString());
                    $variant = $container->getVariant();
                    if ($variant) {
                        $variant = "{$variant}_{$new_variant}";
                    }
                    else {
                        $variant = $new_variant;
                    }
                    Injector::inst()->get(AssetStore::class)->setFromString(
                        $source->toBuffer(),
                        $container->getFilename(),
                        $container->getHash(),
                        $variant
                    );
                    return null;
                });
                if (empty($result)) {
                    $variant = $this->owner->Variant;
                    if ($variant) {
                        $variant = "{$variant}_{$new_variant}";
                    }
                    else {
                        $variant = $new_variant;
                    }
                    $file = DBField::create_field('DBFile', [
                        'Filename' => $this->owner->Filename,
                        'Hash' => $this->owner->Hash,
                        'Variant' => $variant
                    ]);
                    $result = $file->setOriginal($this->owner);
                }
                return $result;
            }
        }
        return $this->owner;
    }
}

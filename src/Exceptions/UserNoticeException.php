<?php

namespace Raise\Common\Exceptions;

class UserNoticeException extends \Exception
{
    public function __toString() {
        return  "Warning: {$this->message} {$this->file} on line {$this->line}";
    }
}

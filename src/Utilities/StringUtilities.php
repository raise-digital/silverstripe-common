<?php

namespace Raise\Common\Utilities;

class StringUtilities
{

    public static function form_slug($value, $separator = '-')
    {
        $slug = str_replace(' ', $separator, strtolower($value));
        $slug = str_replace('&', 'and', $slug);
        $slug = preg_replace('/[^a-z0-9\\' . $separator . ']?/', '', $slug);
        while (stripos($slug, $separator . $separator) !== false) {
            $slug = str_replace($separator . $separator, $separator, $slug);
        }
        return $slug;
    }

    public static function csv_string_to_array($data)
    {
        $csv = array_map('str_getcsv', str_getcsv($data, "\n"));
        array_walk($csv, function (&$element) use ($csv) {
            $element = array_combine($csv[0], $element);
        });
        array_shift($csv);
        return $csv;
    }
}

<?php

namespace Raise\Common\Utilities;

use SilverStripe\Core\Config\Config;

class Geocoder
{
    private static $api_key = '';
    private static $instance;

    public static function inst($instance = null) {
        if (!is_null($instance)) {
            self::$instance = $instance;
        }
        if (is_null(self::$instance)) {
            self::$instance = new Geocoder();
        }
        return self::$instance;
    }

    public function geocode($address, $coords_only = true, $return_response = true)
    {
        $curl = curl_init();
        curl_setopt(
            $curl,
            CURLOPT_URL,
            sprintf(
                'https://maps.googleapis.com/maps/api/geocode/json?key=%s&address=%s&sensor=false',
                Config::inst()->get(Geocoder::class, 'api_key'),
                urlencode($address)
            )
        );
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($curl), false);
        if ($response->status == 'OK' && count($response->results) > 0) {
            return $coords_only ? $response->results[0]->geometry->location : $response->results[0];
        }
        return $return_response ? $response : null;
    }

    public function reverse($latitude, $longitude, $types = array('country'))
    {
        $curl = curl_init();
        curl_setopt(
            $curl,
            CURLOPT_URL,
            sprintf(
                'https://maps.googleapis.com/maps/api/geocode/json?key=%s&latlng=%f,%f&types=%s',
                Config::inst()->get(Geocoder::class, 'api_key'),
                $latitude,
                $longitude,
                implode('|', $types)
            )
        );
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($curl), false);
        if ($response->status == 'OK' && count($response->results) > 0) {
            return $response->results[0]->address_components[0]->long_name;
        }
        return null;
    }
}

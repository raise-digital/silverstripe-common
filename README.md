# SilverStripe Common Utilities Module

## Enabling Optimised Images

Acquire API key from https://tinypng.com/

### Configuration

```yaml
SilverStripe\Assets\Image:
  extensions:
     - Raise\Extensions\OptimisedImage
SilverStripe\Assets\Storage\DBFile:
  extensions:
     - Raise\Extensions\OptimisedImage
---
Only:
  environment: 'test'
---
SilverStripe\Assets\Image:
  tinify_key: 'STAGING-API-KEY'
---
Only:
  environment: 'live'
---
SilverStripe\Assets\Image:
  tinify_key: 'LIVE-API-KEY'
```

### Usage

```php
$image->Compressed();
```

<?php

namespace Raise\Common\Tests\TestModels;

use Raise\Common\Extensions\Meta;

use SilverStripe\Dev\TestOnly;
use SilverStripe\ORM\DataObject;

class MetaTestModel extends DataObject implements TestOnly
{
    private static $table_name = 'MetaTestModel';
    private static $extensions = [
        Meta::class
    ];

    private static $db = [
        'Title' => 'Varchar(200)'
    ];
}

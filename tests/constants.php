<?php

if (array_key_exists('SCRIPT_NAME', $_SERVER) && preg_match('/phpunit$/', $_SERVER['SCRIPT_NAME'])) {
    define('PHPUNIT_RUNNING', true);
    define('ASSETS_DIR', 'assets');
    $tmp = sys_get_temp_dir() . '/ss_tmpfiles_' . time();
    mkdir($tmp);
    define('ASSETS_PATH', $tmp . '/' . ASSETS_DIR);
}
